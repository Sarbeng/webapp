-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2019 at 04:24 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mmda`
--
CREATE DATABASE IF NOT EXISTS `mmda` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mmda`;

-- --------------------------------------------------------

--
-- Table structure for table `assembly`
--

DROP TABLE IF EXISTS `assembly`;
CREATE TABLE `assembly` (
  `idassembly` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `type_of_assembly` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `assembly`:
--

-- --------------------------------------------------------

--
-- Table structure for table `kma_revenue`
--

DROP TABLE IF EXISTS `kma_revenue`;
CREATE TABLE `kma_revenue` (
  `id` int(11) NOT NULL,
  `current_amount` int(11) DEFAULT NULL,
  `current_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `kma_revenue`:
--

-- --------------------------------------------------------

--
-- Table structure for table `market`
--

DROP TABLE IF EXISTS `market`;
CREATE TABLE `market` (
  `idmarket` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `assembly` varchar(45) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `assembly_idassembly` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `market`:
--   `assembly_idassembly`
--       `assembly` -> `idassembly`
--

-- --------------------------------------------------------

--
-- Table structure for table `revenue_head`
--

DROP TABLE IF EXISTS `revenue_head`;
CREATE TABLE `revenue_head` (
  `idrevenue_head` int(11) NOT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `other_names` varchar(45) DEFAULT NULL,
  `assembly` varchar(45) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `assembly_idassembly` int(11) NOT NULL,
  `picture_location` text,
  `picture_key` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `revenue_head`:
--   `assembly_idassembly`
--       `assembly` -> `idassembly`
--

-- --------------------------------------------------------

--
-- Table structure for table `section_head`
--

DROP TABLE IF EXISTS `section_head`;
CREATE TABLE `section_head` (
  `idsection_head` int(11) NOT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `other_names` varchar(45) DEFAULT NULL,
  `market` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `assembly_idassembly` int(11) NOT NULL,
  `revenue_head_idrevenue_head` int(11) NOT NULL,
  `picture_location` text,
  `picture_key` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `section_head`:
--   `assembly_idassembly`
--       `assembly` -> `idassembly`
--   `revenue_head_idrevenue_head`
--       `revenue_head` -> `idrevenue_head`
--

-- --------------------------------------------------------

--
-- Table structure for table `toll_collector`
--

DROP TABLE IF EXISTS `toll_collector`;
CREATE TABLE `toll_collector` (
  `idtoll_collector` int(11) NOT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `other_names` varchar(45) DEFAULT NULL,
  `market` varchar(45) DEFAULT NULL,
  `section` varchar(45) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `section_head_idsection_head` int(11) NOT NULL,
  `assembly_idassembly` int(11) NOT NULL,
  `picture_location` text,
  `picture_key` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `toll_collector`:
--   `assembly_idassembly`
--       `assembly` -> `idassembly`
--   `section_head_idsection_head`
--       `section_head` -> `idsection_head`
--

-- --------------------------------------------------------

--
-- Table structure for table `trader`
--

DROP TABLE IF EXISTS `trader`;
CREATE TABLE `trader` (
  `idtrader` int(11) NOT NULL,
  `surname` varchar(45) DEFAULT NULL,
  `other_names` varchar(45) DEFAULT NULL,
  `shop_name` varchar(45) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `tax_identification_number` int(11) DEFAULT NULL,
  `amount_owing` int(11) DEFAULT '0',
  `amount_paid` int(11) DEFAULT '0',
  `section` varchar(45) DEFAULT NULL,
  `assembly_idassembly` int(11) NOT NULL,
  `market_idmarket` int(11) NOT NULL,
  `paid_for_today` tinyint(1) DEFAULT '0',
  `scanned_for_today` tinyint(1) DEFAULT '0',
  `picture_location` text,
  `picture_key` varchar(100) DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `latitude` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `trader`:
--   `assembly_idassembly`
--       `assembly` -> `idassembly`
--   `market_idmarket`
--       `market` -> `idmarket`
--

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `idTransaction` int(11) NOT NULL,
  `toll_collector` int(11) DEFAULT NULL,
  `market_woman` int(11) DEFAULT NULL,
  `market` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `assembly` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONSHIPS FOR TABLE `transaction`:
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assembly`
--
ALTER TABLE `assembly`
  ADD PRIMARY KEY (`idassembly`);

--
-- Indexes for table `kma_revenue`
--
ALTER TABLE `kma_revenue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `market`
--
ALTER TABLE `market`
  ADD PRIMARY KEY (`idmarket`),
  ADD KEY `fk_market_assembly1_idx` (`assembly_idassembly`);

--
-- Indexes for table `revenue_head`
--
ALTER TABLE `revenue_head`
  ADD PRIMARY KEY (`idrevenue_head`),
  ADD KEY `fk_revenue_head_assembly1_idx` (`assembly_idassembly`);

--
-- Indexes for table `section_head`
--
ALTER TABLE `section_head`
  ADD PRIMARY KEY (`idsection_head`),
  ADD KEY `fk_section_head_assembly1_idx` (`assembly_idassembly`),
  ADD KEY `fk_section_head_revenue_head1_idx` (`revenue_head_idrevenue_head`);

--
-- Indexes for table `toll_collector`
--
ALTER TABLE `toll_collector`
  ADD PRIMARY KEY (`idtoll_collector`),
  ADD KEY `fk_toll_collector_section_head_idx` (`section_head_idsection_head`),
  ADD KEY `fk_toll_collector_assembly1_idx` (`assembly_idassembly`);

--
-- Indexes for table `trader`
--
ALTER TABLE `trader`
  ADD PRIMARY KEY (`idtrader`),
  ADD KEY `fk_trader_assembly1_idx` (`assembly_idassembly`),
  ADD KEY `fk_trader_market1_idx` (`market_idmarket`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`idTransaction`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assembly`
--
ALTER TABLE `assembly`
  MODIFY `idassembly` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `market`
--
ALTER TABLE `market`
  MODIFY `idmarket` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `revenue_head`
--
ALTER TABLE `revenue_head`
  MODIFY `idrevenue_head` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `section_head`
--
ALTER TABLE `section_head`
  MODIFY `idsection_head` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `toll_collector`
--
ALTER TABLE `toll_collector`
  MODIFY `idtoll_collector` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trader`
--
ALTER TABLE `trader`
  MODIFY `idtrader` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `idTransaction` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `market`
--
ALTER TABLE `market`
  ADD CONSTRAINT `fk_market_assembly1` FOREIGN KEY (`assembly_idassembly`) REFERENCES `assembly` (`idassembly`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `revenue_head`
--
ALTER TABLE `revenue_head`
  ADD CONSTRAINT `fk_revenue_head_assembly1` FOREIGN KEY (`assembly_idassembly`) REFERENCES `assembly` (`idassembly`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `section_head`
--
ALTER TABLE `section_head`
  ADD CONSTRAINT `fk_section_head_assembly1` FOREIGN KEY (`assembly_idassembly`) REFERENCES `assembly` (`idassembly`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_section_head_revenue_head1` FOREIGN KEY (`revenue_head_idrevenue_head`) REFERENCES `revenue_head` (`idrevenue_head`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `toll_collector`
--
ALTER TABLE `toll_collector`
  ADD CONSTRAINT `fk_toll_collector_assembly1` FOREIGN KEY (`assembly_idassembly`) REFERENCES `assembly` (`idassembly`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_toll_collector_section_head` FOREIGN KEY (`section_head_idsection_head`) REFERENCES `section_head` (`idsection_head`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trader`
--
ALTER TABLE `trader`
  ADD CONSTRAINT `fk_trader_assembly1` FOREIGN KEY (`assembly_idassembly`) REFERENCES `assembly` (`idassembly`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_trader_market1` FOREIGN KEY (`market_idmarket`) REFERENCES `market` (`idmarket`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
