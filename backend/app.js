var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require("body-parser");
var mysql = require("mysql");
var myConnection = require("express-myconnection");
var app = express();
var dbOptions = require('./aws');

var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');

app.io = require('socket.io')();



app.io = require('socket.io')();

var indexRouter = require('./routes/index')(app.io);
var usersRouter = require('./routes/users')(app.io);
var sectionRouter = require('./routes/section')(app.io);

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));



//settinng up database
app.use(myConnection(mysql,dbOptions,'pool'));

app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//setting up the session
app.use(session({
    secret: 'justasecret',
    resave:true,
    saveUninitialized: true
}));

//setting up passport

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/', sectionRouter);



module.exports = app;
