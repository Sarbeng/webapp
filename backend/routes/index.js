var express = require('express');
var router = express.Router();
//var url = require('url');
//var multer = require('multer');
//var multerS3 = require('multer-s3');
//var aws = require('aws-sdk');
var SqlString = require('sqlstring');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.ejs', { title: 'Express' });
});

//module.exports = router;


//route to Register a new sectional Head 
router.post('/insert_section_heads',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to register a new sectional head
        var sql = "INSERT INTO section_head(surname,other_names,market,section,phone_number,pin,assembly_idassembly,revenue_head_idrevenue_head) VALUES("
        +"(?,?,?,?,?,?,"
        +"(select idassembly from assembly where idassembly like ?),(select idrevenue_head from "
        +"revenue_head where idrevenue_head like ?)) ";;
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,[req.body.surname,req.body.other_names,req.body.market,req.body.section,req.body.phone_number,req.body.pin
                                + req.body.idassembly,req.body.revenue_head],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});



//route to print out sectional head information
router.get('/section_heads',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to get list of sectional heads in desceneding order
        var sql = "Select *from ? order by ? Desc";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,['section_head','idsection_head'],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});



//route to count sectioncal heads per assembly
router.get('/count_section_heads',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to count total section heads for the assembly
        var sql = "SELECT COUNT(idsection_head) as Asokwa FROM section_head where ?;"
        +"SELECT COUNT(idsection_head) as Bantama FROM section_head where ?;"
        +"SELECT COUNT (idsection_head) as Nhyiaeso FROM section_head where ?;"
        +"SELECT COUNT(idsection_head) as Tafo FROM section_head where ?;"
        +"SELECT COUNT(idsection_head) as Suame FROM section_head where ?;"
        +"SELECT COUNT(idsection_head) as Manhyia FROM section_head where ?;"
        +"SELECT COUNT(idsection_head) as Kwadaso FROM section_head where ?;"
        +"SELECT COUNT(idsection_head) as Subin FROM section_head where ?;"
        +"SELECT COUNT(idsection_head) as Oforikrom FROM section_head where ?";

        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,['assembly_idassembly=1','assembly_idassembly=2','assembly_idassembly=3','assembly_idassembly=4','assembly_idassembly=5','assembly_idassembly=6','assembly_idassembly=7','assembly_idassembly=8','assembly_idassembly=9'],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});

//route to register toll collectors information
router.post('/insert_toll_collectors',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to Insert Data of new toll_collectors
        var sql = "INSERT INTO toll_collector (surname,other_names,market,section,phone_number,pin,section_head_idsection_head,assembly_idassembly)" 
          +"VALUES (?,?,?,?,?,?,(select idsection_head from section_head where idsection_head like ?),(select idassembly from assembly where idassembly like ?)) ";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,[req.body.surname,req.body.other_names,req.body.market,req.body.section,req.body.phone_number,req.body.pin,req.body.section,req.body.assembly]
                ,function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});


//route to print out toll collectors information
router.get('/toll_collectors',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to get list of sectional heads in desceneding order
        var sql = "Select *from ? order by ?";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,['toll_collector','idtoll_collector Desc'],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});



//route to count number of toll collectors in a region
router.get('/count_toll_collectors',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to count total toll collectors for the assembly
        var sql = "SELECT COUNT(idtoll_collector) as Asokwa FROM toll_collector where ?;"
        +"SELECT COUNT(idtoll_collector) as Bantama FROM toll_collector where ?;"
        +"SELECT COUNT (idtoll_collector) as Nhyiaeso FROM toll_collector where ?;"
        +"SELECT COUNT(idtoll_collector) as Tafo FROM toll_collector where ?;"
        +"SELECT COUNT(idtoll_collector) as Suame FROM toll_collector where ?;"
        +"SELECT COUNT(idtoll_collector) as Manhyia FROM toll_collector where ?;"
        +"SELECT COUNT(idtoll_collector) as Kwadaso FROM toll_collector where ?;"
        +"SELECT COUNT(idtoll_collector) as Subin FROM toll_collector where ?;"
        +"SELECT COUNT(idtoll_collector) as Oforikrom FROM toll_collector where ?";

        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,['assembly_idassembly=1','assembly_idassembly=2','assembly_idassembly=3','assembly_idassembly=4','assembly_idassembly=5','assembly_idassembly=6','assembly_idassembly=7','assembly_idassembly=8','assembly_idassembly=9'],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});

//getting the total earnings for the day
router.get('/count_total_earning_for_today',function (req,res,next) {
    try{
        //var query = url.parse(req.url,true).query;
        //query to get total earnings for the day
        var sql = "SELECT COUNT(date) as total_earnings_for_today FROM  transaction where ?";
        
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,['DAY(date)= DAY(CURDATE())'],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});


//counting the total earnings per assembly
router.get('/count_total_earning_per_assembly',function (req,res,next) {
    try{
        //var query = url.parse(req.url,true).query;
        //query to count total earnings for the day per each assembly
        var sql = "SELECT COUNT(date) as Asokwa FROM  transaction where DAY(date)= DAY(CURDATE()) AND assembly='1';"
        +"SELECT COUNT(date) as Bantama FROM  transaction where (DAY(date)= DAY(CURDATE())) AND (assembly='2');"
        +"SELECT COUNT(date) as Nhyiaeso FROM  transaction where (DAY(date)= DAY(CURDATE())) AND (assembly='3');"
        +"SELECT COUNT(date) as Tafo  FROM  transaction where DAY(date)= DAY(CURDATE()) AND assembly='4';"
        +"SELECT COUNT(date) as Suame  FROM  transaction where DAY(date)= DAY(CURDATE()) AND assembly='5';"
        +"SELECT COUNT(date) as Manhyia  FROM  transaction where DAY(date)= DAY(CURDATE()) AND assembly='6';"
        +"SELECT COUNT(date) as Kwadaso  FROM  transaction where DAY(date)= DAY(CURDATE()) AND assembly='7';"
        +"SELECT COUNT(date) as Subin FROM  transaction where DAY(date)= DAY(CURDATE()) AND assembly='8';"
        +"SELECT COUNT(date) as Oforikrom  FROM  transaction where DAY(date)= DAY(CURDATE()) AND assembly='9';";

        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,[req.body.assembly],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.send(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});


//getting the total earnings for the year
router.post('/count_total_earning',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;
        //query to get monthly divisions
        var sql = SqlString.format("SELECT COUNT(*) FROM  transaction where date=YEAR(CURRENT_TIMESTAMP) and date=MONTH(CURRENT_TIMESTAMP)");

        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,[],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.send(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});



//printing out the previous amount
router.get('/past_amounts',function (req,res,next) {
    try{
        //var query = url.parse(req.url,true).query;

        var sql = "SELECT *FROM KMA_Revenue where DATE(current_date) = subdate(current_date,1)";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,[],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.send(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});


//printing out the current amount
router.get('/today_amounts',function (req,res,next) {
    try{
        //var query = url.parse(req.url,true).query;

        var sql = "SELECT *FROM KMA_Revenue where DATE(current_date) = DATE(CURRENT_TIMESTAMP())";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,[],function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.send(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});

//Getting the percentage change in previous and current amounts
router.get('/percentage_increase',function (req,res,next) {
    try{
        //var query = url.parse(req.url,true).query;

        var sql = " select t.*,\n" +
            "       (t.current_amount - tprev.current_amount) as diff\n" +
            "from kma_revenue t left join\n" +
            "     kma_revenue tprev\n" +
            "     on tprev.current_date = t.current_date - interval 1 day;";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.send(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});

//getting the various months of the transaction so as to display a grapg in the front end
router.get('/month_analysis',function (req,res,next) {
    try{
        //var query = url.parse(req.url,true).query;
        //query to count the transactions and list them by the various months in a year
        var sql = "SELECT COUNT (date) as january FROM transaction where MONTH(date)=1;"
        +"SELECT COUNT (date) as february FROM transaction where MONTH(date)=2 ;" 
        +"SELECT COUNT (date) as march FROM transaction where MONTH(date)=3;" 
        +"SELECT COUNT (date) as april FROM transaction where MONTH(date)=4;"
        +"SELECT COUNT (date) as may FROM transaction where MONTH(date)=5;"
        +"SELECT COUNT (date) as june FROM transaction where MONTH(date)=6;" 
        +"SELECT COUNT (date) as july FROM transaction where MONTH(date)=7;"
        +"SELECT COUNT (date) as august FROM transaction where MONTH(date)=8 ;"
        +"SELECT COUNT (date) as september FROM transaction where MONTH(date)=9;" 
        +"SELECT COUNT (date) as october FROM transaction where MONTH(date)=10;"
        +"SELECT COUNT (date) as november FROM transaction where MONTH(date)=11;"
        +"SELECT COUNT (date) as december FROM transaction where MONTH(date)=12";

        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.send(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});





module.exports = function (io) {
    //Socket.IO
    io.on('connection', function (socket) {
        console.log('User has connected to Index');
        //ON Events
        socket.on('admin', function () {
            console.log('Successful Socket Test');
        });

        //End ON Events
    });
    return router;
};


