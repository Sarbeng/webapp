var express = require('express');
var router = express.Router();

//with this route the section head should be able to 
//track the amount of money collected in his section


//route to count todays money per section
router.post('/count_todays_money_collected_per_section',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to count amount of money per section
        var sql = "SELECT COUNT(date) as Todays_Money FROM  transaction where (DAY(date)= DAY(CURDATE())) AND (section='"+req.body.section+"')";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});


//route to count yesterdays money per section
router.post('/count_yesterdays_money_collected_per_section',function (req,res,next) {
    try{
       // var query = url.parse(req.url,true).query;

        //query to count amount of money per section
        var sql = "SELECT COUNT(date) as Yesterdays_Money FROM  transaction where DATE(date) = subdate(date,1) AND (section='"+req.body.section+"')";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.json(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});

//Getting the percentage change in previous and current amounts
router.get('/section_percentage_increase',function (req,res,next) {
    try{
        //var query = url.parse(req.url,true).query;

        var sql = " select t.*,\n" +
            "       (t.current_amount - tprev.current_amount) as diff\n" +
            "from kma_revenue t left join\n" +
            "     kma_revenue tprev\n" +
            "     on tprev.current_date = t.current_date - interval 1 day;";
        //setting up connectionn to database
        req.getConnection(function(err,connection){
            if (err) return next(err);

            connection.query(sql,function(err,rows){
                if (err) return next (err);
                //store database elements into array
                var resEmp=[];
                for (var rowIndex in rows){
                    var rowObj = rows[rowIndex];
                    resEmp.push(rowObj);
                }
                res.send(resEmp);

            })

        });
    }
    catch(ex){
        console.log("Internal Error",ex);
    }
});

module.exports = function (io) {
    //Socket.IO
    io.on('connection', function (socket) {
        console.log('User has connected to Index');
        //ON Events
        socket.on('admin', function () {
            console.log('Successful Socket Test');
        });

        //End ON Events
    });
    return router;
};


