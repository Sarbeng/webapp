var express = require('express');
var router = express.Router();
var url = require('url');
var multer = require('multer');
var multerS3 = require('multer-s3');
var aws = require('aws-sdk');
var SqlString = require('sqlstring');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var LocalStrategy = require("passport-local").Strategy;
var passport = require('passport');
var app = express();


function isLoggedIn(req, res, next){
    if(req.isAuthenticated())
        return next();

    res.redirect('/');
}

passport.serializeUser(function(user, done){
    done(null, user.id);
});

passport.deserializeUser(function(id, done){
    connection.query("SELECT * FROM revenue_head WHERE id = ? ", [id],
        function(err, rows){
            done(err, rows[0]);
        });
});

passport.use(
    'local-signup',
    new LocalStrategy({
            usernameField : 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done){
            connection.query("SELECT * FROM users WHERE username = ? ",
                [username], function(err, rows){
                    if(err)
                        return done(err);
                    if(rows.length){
                        return done(null, false, req.flash('signupMessage', 'That is already taken'));
                    }else{
                        var newUserMysql = {
                            username: username,
                            password: bcrypt.hashSync(password, null, null)
                        };

                        var insertQuery = "INSERT INTO users (username, password) values (?, ?)";

                        connection.query(insertQuery, [newUserMysql.username, newUserMysql.password],
                            function(err, rows){
                                newUserMysql.id = rows.insertId;

                                return done(null, newUserMysql);
                            });
                    }
                });
        })
);

passport.use(
    'local-login',
    new LocalStrategy({
            usernameField : 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done){
            connection.query("SELECT * FROM users WHERE username = ? ", [username],
                function(err, rows){
                    if(err)
                        return done(err);
                    if(!rows.length){
                        return done(null, false, req.flash('loginMessage', 'No User Found'));
                    }
                    if(!bcrypt.compareSync(password, rows[0].password))
                        return done(null, false, req.flash('loginMessage', 'Wrong Password'));

                    return done(null, rows[0]);
                });
        })
);



//login page
app.get('/login', function(req, res){
    res.render('login.ejs', {message:req.flash('loginMessage')});
});

//user.login
router.post('/login',passport.authenticate('local-login',{
    successRedirect:'/profile',
    failureRedirect: '/login',
    failureFlash: true

}),function(req, res){
        if(req.body.remember){
            req.session.cookie.maxAge = 1000 * 60 * 3;
        }else{
            req.session.cookie.expires = false;
        }
        res.redirect('/');
    }
    );


//showing signup page
router.get('/signup', function(req, res){
    res.render('signup.ejs', {message: req.flash('signupMessage')});
});

router.post('/signup', passport.authenticate('local-signup', {
    successRedirect: '/profile',
    failureRedirect: '/signup',
    failureFlash: true
}));

//show profile page
router.get('/profile', isLoggedIn, function(req, res){
    res.render('profile.ejs', {
        user:req.user
    });
});

router.get('/logout', function(req,res){
    req.logout();
    res.redirect('/');
});

/**
 * PROFILE IMAGE STORING STARTS
 */
const s3 = new aws.S3({
    accessKeyId: 'AKIAJVHPC4AFMFIXMZMQ',
    secretAccessKey: '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',
    Bucket: 'mmdabucket',
    region:'us-east-2'
});

/**
 * Single Upload
 */
const profileImgUpload = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'mmdabucket',
        acl: 'public-read',
        key: function (req, file, cb) {
            cb(null, path.basename( file.originalname, path.extname( file.originalname ) ) + '-' + Date.now() + path.extname( file.originalname ) )
        }
    }),
    limits:{ fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
    fileFilter: function( req, file, cb ){
        checkFileType( file, cb );
    }
}).single('profileImage');


/**
 * Check File Type
 * @param file
 * @param cb
 * @return {*}
 */
function checkFileType( file, cb ){
    // Allowed ext
    const filetypes = /jpeg|jpg|png|gif/;
    // Check ext
    const extname = filetypes.test( path.extname( file.originalname ).toLowerCase());
    // Check mime
    const mimetype = filetypes.test( file.mimetype );
    if( mimetype && extname ){
        return cb( null, true );
    } else {
        cb( 'Error: Images Only!' );
    }
}

/**
 * @route POST api/profile/business-img-upload
 * @desc Upload post image
 * @access public
 */
router.post( '/profile-img-upload', ( req, res ) => {
    profileImgUpload(req, res, (error) => {
        // console.log( 'requestOkokok', req.file );
        // console.log( 'error', error );
        if (error) {
            console.log('errors', error);
            res.json({error: error});
        } else {
            // If File not found
            if (req.file === undefined) {
                console.log('Error: No File Selected!');
                res.json('Error: No File Selected');
            } else {
                // If Success
                const imageName = req.file.key;
                const imageLocation = req.file.location;
// Save the file name into database into profile model

                req.getConnection(function (err, con, next) {
                    if (err) {
                        console.error('SQL Connection error: ', err);
                        return next(err);
                    } else {
                        var sql =SqlString.format( "INSERT INTO section_head(surname,other_names,market,section,phone_number,pin,revenue_head_idrevenue_head,assembly_idassembly,picture_location,picture_key)" +
                            "  Values (?,?,?,?,?,?," +
                            "(SELECT idassembly from assembly where idassembly = ? ),(SELECT idrevenue_head from revenue_head where idrevenue_head = ? )," +
                            "?,?) ");


                        con.query(sql,[req.body.surname,req.body.other_names,req.body.market,req.body.section,req.body.phone_number,bcrypt.hashSync(req.body.pin,null,null),
                            req.body.idassembly, req.body.idrevenue_head,req.file.location,req.file.key], function (err, rows, fields) {
                            if (err) {
                                console.error('SQL error: ', err);
                                return next(err);
                            }
                            var resEmp = [];
                            for (var newsIndex in rows) {
                                var newsObj = rows[newsIndex];
                                resEmp.push(newsObj);
                            }
                            //res.json(resEmp);
                        });

                        res.json({
                            image: imageName,
                            location: imageLocation,
                            //  resEmp: resEmp
                        });
                    }
                })
            }
        }
// End of single profile upload

    });
});


//registering new toll collectors
router.post( '/toll_collectors', ( req, res ) => {
    profileImgUpload(req, res, (error) => {
        // console.log( 'requestOkokok', req.file );
        // console.log( 'error', error );
        if (error) {
            console.log('errors', error);
            res.json({error: error});
        } else {
            // If File not found
            if (req.file === undefined) {
                console.log('Error: No File Selected!');
                res.json('Error: No File Selected');
            } else {
                // If Success
                const imageName = req.file.key;
                const imageLocation = req.file.location;
// Save the file name into database into profile model

                req.getConnection(function (err, con, next) {
                    if (err) {
                        console.error('SQL Connection error: ', err);
                        return next(err);
                    } else {
                        var sql =SqlString.format("INSERT INTO toll_collectors(surname,other_names,market,section,phone_number,pin,section_head_idsection_head,assembly_idassembly,picture_location,picture_key) VALUES(" +
                            "?,?,?,?,?,?,?,?,?,?)");


                        con.query(sql,[req.body.surname,req.body.other_names,req.body.market,req.body.section,req.body.phone_number,req.body.pin,
                            req.body.idassembly,req.file.location,req.file.key], function (err, rows, fields) {
                            if (err) {
                                console.error('SQL error: ', err);
                                return next(err);
                            }
                            var resEmp = [];
                            for (var newsIndex in rows) {
                                var newsObj = rows[newsIndex];
                                resEmp.push(newsObj);
                            }
                            //res.json(resEmp);
                        });

                        res.json({
                            image: imageName,
                            location: imageLocation,
                            //  resEmp: resEmp
                        });
                    }
                })
            }
        }
// End of single profile upload

    });
});


//module.exports = router;
module.exports = function (io) {
    //Socket.IO
    io.on('connection', function (socket) {
        console.log('User has connected to Index');
        //ON Events
        socket.on('admin', function () {
            console.log('Successful Socket Test');
        });

        //End ON Events
    });
    return router;
};